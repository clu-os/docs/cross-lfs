#! /bin/sh

cd BOOK
make \
	RENDERDIR=../temp \
	HTMLDIR=../public \
	NOCHUNKDIR=../public \
	DLLISTDIR=../public \
	HTML_EXT=xhtml \
	-j 2 \
	html nochunks download-list md5-list
