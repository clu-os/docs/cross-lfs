# Cross Linux From Scratch

Read the latest [CI/CD](https://en.wikipedia.org/wiki/CI/CD) version online:

- [HTML (separate pages)](https://clu-os.gitlab.io/docs/cross-lfs/)

| 32bit | Multilib | 64bit-only  |
|:-----:|:--------:|:-----------:|
| [mips]| [mips64] | [mips64-64] |
| [ppc] | [ppc64]  | [ppc64-64]  |
|[sparc]| [sparc64]| [sparc64-64]|
| [x86] | [x86_64] | [x86_64-64] |

- HTML (single page):

|    32bit    |    Multilib    |    64bit-only     |
|:-----------:|:--------------:|:-----------------:|
| [mips.xhtml]| [mips64.xhtml] | [mips64-64.xhtml] |
| [ppc.xhtml] | [ppc64.xhtml]  | [ppc64-64.xhtml]  |
|[sparc.xhtml]| [sparc64.xhtml]| [sparc64-64.xhtml]|
| [x86.xhtml] | [x86_64.xhtml] | [x86_64-64.xhtml] |

- PDF

Changes since the last official release:

- [x] [Link relationships](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/rel) have been re-enabled (`prev`, `next`, `up`, `home`, `license`)
- [x] [Open Graph](https://ogp.me/) metadata has been added, social timeline cards support (`<meta property="og:*">`)
- [x] Support for configurable filename extension has been re-enabled ([html.ext](https://docbook.sourceforge.net/release/xsl/current/doc/html/html.ext.html))
- [ ] Support for configurable output character encoding ([chunker.output.encoding](https://docbook.sourceforge.net/release/xsl/current/doc/html/chunker.output.encoding.html))
- [x] Some filenames are based on element `id` ([use.id.as.filename](https://docbook.sourceforge.net/release/xsl/current/doc/html/use.id.as.filename.html))
- [ ] Parallel `make`, `WIP`
- [ ] Incremental `make`, `WIP`
- [x] Same `make` targets for all books (`html`, `pdf`, `nochunks`, ...)
- [x] CSS stylesheets have been combined, for all media (`screen` and `print`)
- [ ] "Dark Mode", `TODO`

Visit the official, upstream, pages:

- [Cross-Linux From Scratch on GitHub](https://github.com/cross-lfs)
- [CLFS Trac ](https://trac.clfs.org/)



[mips]:        https://clu-os.gitlab.io/docs/cross-lfs/mips/
[mips64]:      https://clu-os.gitlab.io/docs/cross-lfs/mips64/
[mips64-64]:   https://clu-os.gitlab.io/docs/cross-lfs/mips64-64/
[ppc]:         https://clu-os.gitlab.io/docs/cross-lfs/ppc/
[ppc64]:       https://clu-os.gitlab.io/docs/cross-lfs/ppc64/
[ppc64-64]:    https://clu-os.gitlab.io/docs/cross-lfs/ppc64-64/
[sparc]:       https://clu-os.gitlab.io/docs/cross-lfs/sparc/
[sparc64]:     https://clu-os.gitlab.io/docs/cross-lfs/sparc64/
[sparc64-64]:  https://clu-os.gitlab.io/docs/cross-lfs/sparc64-64/
[x86]:         https://clu-os.gitlab.io/docs/cross-lfs/x86/
[x86_64]:      https://clu-os.gitlab.io/docs/cross-lfs/x86_64/
[x86_64-64]:   https://clu-os.gitlab.io/docs/cross-lfs/x86_64-64/

[mips.xhtml]:        https://clu-os.gitlab.io/docs/cross-lfs/CLFS-mips.xhtml
[mips64.xhtml]:      https://clu-os.gitlab.io/docs/cross-lfs/CLFS-mips64.xhtml
[mips64-64.xhtml]:   https://clu-os.gitlab.io/docs/cross-lfs/CLFS-mips64-64.xhtml
[ppc.xhtml]:         https://clu-os.gitlab.io/docs/cross-lfs/CLFS-ppc.xhtml
[ppc64.xhtml]:       https://clu-os.gitlab.io/docs/cross-lfs/CLFS-ppc64.xhtml
[ppc64-64.xhtml]:    https://clu-os.gitlab.io/docs/cross-lfs/CLFS-ppc64-64.xhtml
[sparc.xhtml]:       https://clu-os.gitlab.io/docs/cross-lfs/CLFS-sparc.xhtml
[sparc64-64.xhtml]:  https://clu-os.gitlab.io/docs/cross-lfs/CLFS-sparc64-64.xhtml
[sparc64.xhtml]:     https://clu-os.gitlab.io/docs/cross-lfs/CLFS-sparc64.xhtml
[x86.xhtml]:         https://clu-os.gitlab.io/docs/cross-lfs/CLFS-x86.xhtml
[x86_64.xhtml]:      https://clu-os.gitlab.io/docs/cross-lfs/CLFS-x86_64.xhtml
[x86_64-64.xhtml]:   https://clu-os.gitlab.io/docs/cross-lfs/CLFS-x86_64-64.xhtml
