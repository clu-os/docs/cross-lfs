<?xml version='1.0' encoding='ISO-8859-1'?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

  <!-- Second level chunked output template.
       Sets global params and include customized elements templates. -->

  <!-- Upstream XHTML presentation templates -->
  <xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/docbook.xsl"/>

  <!-- Use ISO-8859-1 for output instead of default UTF-8, requires the XML declaration -->
  <xsl:param name="chunker.output.method" select="'xml'"/>
  <xsl:param name="chunker.output.encoding" select="'ISO-8859-1'"/>
  <xsl:param name="chunker.output.media-type" select="'application/xhtml+xml'"/>
  <xsl:param name="chunker.output.doctype-public" select="'-//W3C//DTD XHTML 1.0 Strict//EN'"/>
  <xsl:param name="chunker.output.doctype-system" select="'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'"/>
  <xsl:param name="chunker.output.omit-xml-declaration" select="'no'"/>
  <xsl:param name="chunker.output.indent" select="'no'"/>
  <xsl:param name="use.id.as.filename" select="'1'"/>
  <xsl:param name="generate.legalnotice.link" select="'1'"/>
  <xsl:param name="html.head.legalnotice.link.types" select="'license'"/>

  <!-- Including our customized elements templates -->
  <xsl:include href="common.xsl"/>
  <xsl:include href="xhtml/lfs-admon.xsl"/>
  <xsl:include href="xhtml/lfs-mixed.xsl"/>
  <xsl:include href="xhtml/lfs-sections.xsl"/>
  <xsl:include href="xhtml/lfs-titles.xsl"/>
  <xsl:include href="xhtml/lfs-toc.xsl"/>
  <xsl:include href="xhtml/lfs-xref.xsl"/>

  <xsl:param name="html.stylesheet" select="'stylesheets/lfs.css'"/>
  <xsl:template name='user.head.content'>
    <xsl:variable name="description" select="./para[position()=1] | ./sect2[@role='package']/para[position()=1] | ./sect2[position()=1]/para[position()=1]"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="twitter:card" content="summary"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title">
      <xsl:attribute name="content">
        <xsl:value-of select="//bookinfo/title"/>
        <xsl:if test='./title'>
          <xsl:text>: </xsl:text><xsl:value-of select="./title"/>
        </xsl:if>
      </xsl:attribute>
    </meta>
    <xsl:if test="0 &lt; string-length( $description )">
      <meta property="og:description">
        <xsl:attribute name="content">
          <xsl:choose>
            <xsl:when test="160 &lt; string-length( $description )">
              <xsl:value-of select="substring( $description, 1, 160 )"/>
              <xsl:text>...</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$description"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
      </meta>
    </xsl:if>
  </xsl:template>

  <!-- Drop some unwanted style attributes -->
  <xsl:param name="ulink.target" select="''"/>
  <xsl:param name="css.decoration" select="0"/>

  <!-- Control generation of ToCs and LoTs -->
  <xsl:param name="generate.toc">
    book      toc,title
    preface   toc
    part      toc
    chapter   toc
    appendix  nop
    sect1     nop
    sect2     nop
    sect3     nop
    sect4     nop
    sect5     nop
    section   nop
  </xsl:param>

  <!-- How deep should recursive sections appear in the TOC? -->
  <xsl:param name="toc.section.depth">1</xsl:param>

  <!-- How deep, at most, should each TOC be? -->
  <xsl:param name="toc.max.depth">3</xsl:param>

</xsl:stylesheet>
